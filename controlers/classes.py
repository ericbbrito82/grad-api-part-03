import json
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_session
from domain.schemas import Class as class_schemas
from domain.schemas.student import Student
from services import class_service
from uuid import UUID
import json

router = APIRouter()

TAG = 'Classes'


@router.get(   # Buscar todas as disciplinas
    '/classes',
    summary='Fetch all classes data',
    tags=[TAG],
    response_model=list[class_schemas.Class]
)
def read_classes(db: Session = Depends(get_session)):
    return class_service.get_classes(db)


@router.get(   # Rankear as disciplinas de acordo com a quantidade de alunos
    '/classes/ranking',
    summary='Get a ranking of classes by the number of their enrolled students',
    tags=[TAG],
    response_model=list[class_schemas.ClassRank]
)
def read_classes_ranking(db: Session = Depends(get_session)):
    return class_service.rank_classes(db)


@router.get(   # Buscar uma disciplina específica
    '/classes/{class_id}',
    summary='Fetch a class data',
    tags=[TAG],
    response_model=class_schemas.Class
)
def read_class(class_id: UUID, db: Session = Depends(get_session)):
    return class_service.get_class(class_id, db)


@router.post(   # Adicionar uma nova disciplina
    '/classes',
    summary='Add a new class',
    tags=[TAG],
    status_code=201,
    response_model=class_schemas.Class
)
def create_class(class_data: class_schemas.ClassData, db: Session = Depends(get_session)):
    return class_service.add_class(class_data, db)


@router.get(   # Buscar os alunos de uma disciplina
    '/classes/{class_id}/students',
    summary='Fetch students of a class',
    tags=[TAG],
    response_model=list[Student]
)
def read_students_class(class_id: UUID, db: Session = Depends(get_session)):
    return class_service.get_students_class(class_id, db)


@router.put(   # Alterar o professor de uma disciplina
    '/classes/{classes_id}/teacher/{professor_id}',
    summary='Set or change a professor as the teacher of a class',
    tags=[TAG],
    response_model=class_schemas.Class
)
def change_teacher_class(class_id: UUID, professor_id: UUID, db: Session = Depends(get_session)):
    return class_service.set_teacher_class(class_id, professor_id, db)
