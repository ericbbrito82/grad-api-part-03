from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_session
from domain.schemas import student as student_schemas
from services import student_service
from uuid import UUID

router = APIRouter()

TAG = 'Students'


@router.get(   # Buscar todos os estudantes
    '/students',
    summary='Fetch all students data',
    tags=[TAG],
    response_model=list[student_schemas.Student]
)
def read_students(db: Session = Depends(get_session)):
    return student_service.get_students(db)


@router.get(   # Buscar um estudante específico
    '/students/{student_id}',
    summary='Fetch student data',
    tags=[TAG],
    response_model=student_schemas.Student
)
def read_student(student_id: UUID, db: Session = Depends(get_session)):
    return student_service.get_student(student_id, db)


@router.post(   # Adicionar um novo estudante
    '/students',
    summary='Add a new student',
    tags=[TAG],
    status_code=201,
    response_model=student_schemas.Student
)
def create_student(student_data: student_schemas.StudentData, db: Session = Depends(get_session)):
    return student_service.add_student(student_data, db)


@router.put(   # Alterar o curso de um estudante
    '/students/{student_id}/course/{course_id}',
    summary='Set or change a students course',
    tags=[TAG],
    response_model=student_schemas.Student
)
def change_student_course(student_id: UUID, course_id: UUID, db: Session = Depends(get_session)):
    return student_service.set_student_course(student_id, course_id, db)


@router.post(  # Matriculando um aluno numa matéria
    '/students/{student_id}/class/{class_id}',
    summary='Enroll student in a class',
    tags=[TAG],
    response_model=student_schemas.Student
)
def enroll_student_class(student_id: UUID, class_id: UUID, db: Session = Depends(get_session)):
    student_service.add_student_class(student_id, class_id, db)
    return student_service.get_student(student_id, db)


@router.delete(   # Removendo uma materia de um estudante
    '/students/{student_id}/class/{class_id}',
    summary='Remove student from a class',
    tags=[TAG],
    response_model=student_schemas.Student
)
def delete_student_class(student_id: UUID, class_id: UUID, db: Session = Depends(get_session)):
    student_service.remove_student_class(student_id, class_id, db)
    return student_service.get_student(student_id, db)
