from sqlalchemy.orm import Session

from domain.models.students import Students
from domain.models.studentsclasses import StudentsClasses

from uuid import UUID


def get_students(db: Session):
    return db.query(Students).all()


def get_student(student_id: UUID, db: Session):
    return db.query(Students).filter(Students.id == student_id).first()


def get_student_classes(student_id: UUID, db: Session):
    return db.query(StudentsClasses).filter(StudentsClasses.student_id == student_id).all()


def add_student(new_student: Students, db: Session):
    db.add(new_student)
    db.commit()
    db.refresh(new_student)
    return db.query(Students).filter(Students.id == new_student.id).first()


def set_student_course(student_id: UUID, course_id: UUID, db: Session):
    db.query(Students).filter(Students.id == student_id).update(
        {Students.course_id: course_id})
    db.commit()
    return db.query(Students).filter(Students.id == student_id).first()


def add_student_class(student_id: UUID, class_id: UUID, db: Session):
    new_enroll = StudentsClasses(student_id=student_id, class_id=class_id)
    db.add(new_enroll)
    db.commit()
    db.refresh(new_enroll)
    return new_enroll


def remove_student_class(student_id: UUID, class_id: UUID, db: Session):
    db.query(StudentsClasses).filter(
        StudentsClasses.student_id == student_id,
        StudentsClasses.class_id == class_id).delete()
    db.commit()
    return
