from sqlalchemy.orm import Session

from domain.models.students import Students
from domain.models.courses import Courses

from uuid import UUID


def get_courses(db: Session):
    return db.query(Courses).all()


def get_course(course_id: UUID, db: Session):
    return db.query(Courses).filter(Courses.id == course_id).first()


def add_course(new_course: Courses, db: Session):
    db.add(new_course)
    db.commit()
    db.refresh(new_course)
    return db.query(Courses).filter(Courses.id == new_course.id).first()


def get_students_course(course_id: UUID, db: Session):
    return db.query(Students).filter(Students.course_id == course_id).all()


def set_course_coordinator(course_id: UUID, professor_id: UUID, db: Session):
    db.query(Courses).filter(Courses.id == course_id).update(
        {Courses.coordinator_id: professor_id})
    db.commit()
    return db.query(Courses).filter(Courses.id == course_id).first()
