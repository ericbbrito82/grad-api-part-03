from sqlalchemy import Column, ForeignKey, String
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid

from domain.models.generic import GenericBase


class Classes(GenericBase):
    __tablename__ = "classes"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, unique=True)
    code = Column(String)
    description = Column(String)
    professor_id = Column(UUID(as_uuid=True), ForeignKey("professors.id"))

    professor = relationship("Professors", back_populates="classes")
    class_students = relationship(
        "StudentsClasses", back_populates="link_class")
