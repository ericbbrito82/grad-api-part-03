from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.models.students import Students
from domain.models.studentsclasses import StudentsClasses

from domain.schemas import student as student_schemas

from repositories import student_repository

from services.class_service import get_class

from uuid import UUID


def get_students(db: Session):
    students = student_repository.get_students(db)
    for student in students:
        student.classes = []
        classes = student_repository.get_student_classes(student.id, db)
        for class_n in classes:
            student.classes.append(get_class(class_n.class_id, db).name)
    return student_repository.get_students(db)


def get_student(student_id: UUID, db: Session):
    student = student_repository.get_student(student_id, db)
    if student is None:
        raise HTTPException(
            status_code=404, detail=f'Student with id={student_id} not found')
    student.classes = []
    classes = student_repository.get_student_classes(student_id, db)
    for class_n in classes:
        student.classes.append(get_class(class_n.class_id, db).name)
    return student


def add_student(student_data: student_schemas.StudentData, db: Session):
    new_student = Students(**student_data.dict())
    return student_repository.add_student(new_student, db)


def set_student_course(student_id: UUID, course_id: UUID, db: Session):
    get_student(student_id, db)
    return student_repository.set_student_course(student_id, course_id, db)


def add_student_class(student_id: UUID, class_id: UUID, db: Session):
    return student_repository.add_student_class(student_id, class_id, db)


def remove_student_class(student_id: UUID, class_id: UUID, db: Session):
    return student_repository.remove_student_class(student_id, class_id, db)
