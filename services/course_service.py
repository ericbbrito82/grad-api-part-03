from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.models.courses import Courses

from domain.schemas.course import CourseData

from repositories import course_repository, student_repository
from services.class_service import get_class

from uuid import UUID


def get_courses(db: Session):
    return course_repository.get_courses(db)


def get_course(course_id: UUID, db: Session):
    f = course_repository.get_course(course_id, db)
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Course with id={course_id} not found')
    return f


def add_course(course_data: CourseData, db: Session):
    new_course = Courses(**course_data.dict())
    return course_repository.add_course(new_course, db)


def get_students_course(course_id: UUID, db: Session):
    students = course_repository.get_students_course(course_id, db)
    for student in students:
        student.classes = []
        classes = student_repository.get_student_classes(student.id, db)
        for class_n in classes:
            student.classes.append(get_class(class_n.class_id, db).name)
    return course_repository.get_students_course(course_id, db)


def set_course_coordinator(course_id: UUID, professor_id: UUID, db: Session):
    return course_repository.set_course_coordinator(course_id, professor_id, db)
