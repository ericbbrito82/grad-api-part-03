from array import array
from typing_extensions import Required
from sqlalchemy import Column, ForeignKey, String, Date, ARRAY
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid

from domain.models.generic import GenericBase
from domain.schemas import Class as class_schemas


class Students(GenericBase):
    __tablename__ = "students"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    cpf = Column(String, unique=True)
    name = Column(String)
    birth_date = Column(Date)
    rg_number = Column(String)
    rg_expediter = Column(String)
    rg_uf = Column(String)
    course_id = Column(UUID(as_uuid=True), ForeignKey("courses.id"))

    student_classes = relationship(
        "StudentsClasses", back_populates="link_student")
