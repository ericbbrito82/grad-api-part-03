from pydantic import Field
from domain.schemas.generic import GenericModel
from uuid import UUID


class ProfessorData(GenericModel):
    cpf: str = Field(title="Professor's CPF",
                     regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}', Example="123.456.789-01")
    name: str = Field(title="Professor's name", Example="John Doe")
    title: str = Field(title="Professor's Title", Example="PhD")


class Professor(ProfessorData):
    id: UUID = Field(title="Professor ID", Example="UUID")

    class Config:
        orm_mode = True
