from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid

from domain.models.generic import GenericBase


class StudentsClasses(GenericBase):
    __tablename__ = "students_classes"

    student_id = Column(UUID(as_uuid=True), ForeignKey(
        "students.id"))
    class_id = Column(UUID(as_uuid=True), ForeignKey(
        "classes.id"))
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)

    link_student = relationship("Students", back_populates="student_classes")
    link_class = relationship("Classes", back_populates="class_students")
