from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.models.classes import Classes

from domain.schemas import Class as class_schemas

from repositories import class_repository
from services import student_service

from uuid import UUID


def get_classes(db: Session):
    return class_repository.get_classes(db)


def get_class(class_id: UUID, db: Session):
    f = class_repository.get_class(class_id, db)
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Class with id={class_id} not found')
    return f


def add_class(class_data: class_schemas.ClassData, db: Session):
    new_class = Classes(**class_data.dict())
    return class_repository.add_class(new_class, db)


def get_students_class(class_id: UUID, db: Session):
    x = []
    for f in class_repository.get_students_class(class_id, db):
        x.append(student_service.get_student(f.student_id, db))
    return x


def set_teacher_class(class_id: UUID, professor_id: UUID, db: Session):
    return class_repository.set_teacher_class(class_id, professor_id, db)


def rank_classes(db: Session):
    rank = []
    classes = class_repository.get_classes(db)
    for class_n in classes:
        n_of_students = class_repository.count_students_class(class_n.id, db)
        rank.append(class_schemas.ClassRank(
            class_name=class_n.name, number_of_students=n_of_students))
    rank = sorted(rank, key=lambda y: y.number_of_students, reverse=True)
    return rank
