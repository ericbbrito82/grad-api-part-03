from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_session
from domain.schemas import professor as professor_schemas
from services import professor_service
from uuid import UUID

router = APIRouter()

TAG = 'Professors'


@router.get(   # Buscar todos os professores
    '/professors',
    summary='Fetch all professors data',
    tags=[TAG],
    response_model=list[professor_schemas.Professor]
)
def read_professors(db: Session = Depends(get_session)):
    return professor_service.get_professors(db)


@router.get(   # Buscar um professor específico
    '/professors/{professor_id}',
    summary='Fetch professor data',
    tags=[TAG],
    response_model=professor_schemas.Professor
)
def read_professor(professor_id: UUID, db: Session = Depends(get_session)):
    return professor_service.get_professor(professor_id, db)


@router.post(   # Adicionar um novo professor
    '/professors',
    summary='Add a new professor',
    tags=[TAG],
    status_code=201,
    response_model=professor_schemas.Professor
)
def create_professor(professor_data: professor_schemas.ProfessorData, db: Session = Depends(get_session)):
    return professor_service.add_professor(professor_data, db)
