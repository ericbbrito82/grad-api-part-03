from fastapi import FastAPI
import uvicorn

from config.server import port
from controlers import routes

app = FastAPI(
    title="PROGRAD API",
    version="0.0.3"
)

for router in routes:
    app.include_router(router)


if __name__ == '__main__':
    uvicorn.run(app='main:app', host='localhost', port=port, reload=True)
