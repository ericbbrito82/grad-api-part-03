from sqlalchemy.orm import Session

from domain.models.professors import Professors

from uuid import UUID


def get_professors(db: Session):
    return db.query(Professors).all()


def get_professor(professor_id: UUID, db: Session):
    return db.query(Professors).filter(
        Professors.id == professor_id).first()


def add_professor(new_professor: Professors, db: Session):
    db.add(new_professor)
    db.commit()
    db.refresh(new_professor)
    return db.query(Professors).filter(
        Professors.id == new_professor.id).first()
