from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_session
from domain.schemas import course as course_schemas
from domain.schemas.student import Student
from services import course_service
from uuid import UUID

router = APIRouter()

TAG = 'Courses'


@router.get(   # Buscar todos os cursos
    '/courses',
    summary='Fetch all courses data',
    tags=[TAG],
    response_model=list[course_schemas.Course]
)
def read_courses(db: Session = Depends(get_session)):
    return course_service.get_courses(db)


@router.get(   # Buscar um curso específico
    '/courses/{course_id}',
    summary='Fetch course data',
    tags=['Courses'],
    response_model=course_schemas.Course
)
def read_course(course_id: UUID, db: Session = Depends(get_session)):
    return course_service.get_course(course_id, db)


@router.post(   # Adicionar um novo curso
    '/courses',
    summary='Add a new course',
    tags=[TAG],
    status_code=201,
    response_model=course_schemas.Course
)
def create_course(course_data: course_schemas.CourseData, db: Session = Depends(get_session)):
    return course_service.add_course(course_data, db)


@router.get(   # Buscar alunos de um curso específico
    '/courses/{course_id}/students',
    summary='Fetch students of course',
    tags=[TAG],
    response_model=list[Student]
)
def read_students_course(course_id: UUID, db: Session = Depends(get_session)):
    return course_service.get_students_course(course_id, db)


@router.put(   # Alterar o coordenador de um curso
    '/courses/{course_id}/coordinator/{professor_id}',
    summary='Set or change a professor as the course coordinator',
    tags=[TAG],
    response_model=course_schemas.Course
)
def change_course_coordinator(course_id: UUID, professor_id: UUID, db: Session = Depends(get_session)):
    return course_service.set_course_coordinator(course_id, professor_id, db)
