from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid

from domain.models.generic import GenericBase


class Professors(GenericBase):
    __tablename__ = "professors"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    cpf = Column(String, unique=True)
    name = Column(String)
    title = Column(String)

    classes = relationship("Classes", back_populates="professor")
