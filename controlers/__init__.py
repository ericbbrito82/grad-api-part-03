from . import students, professors, courses, classes

routes = [
    students.router,
    professors.router,
    courses.router,
    classes.router
]
