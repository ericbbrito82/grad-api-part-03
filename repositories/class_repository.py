from sqlalchemy.orm import Session

from domain.models.classes import Classes
from domain.models.studentsclasses import StudentsClasses

from uuid import UUID


def get_classes(db: Session):
    return db.query(Classes).all()


def get_class(class_id: UUID, db: Session):
    return db.query(Classes).filter(Classes.id == class_id).first()


def add_class(new_class: Classes, db: Session):
    db.add(new_class)
    db.commit()
    db.refresh(new_class)
    return db.query(Classes).filter(Classes.id == new_class.id).first()


def get_students_class(class_id: UUID, db: Session):
    return db.query(StudentsClasses).filter(StudentsClasses.class_id == class_id).all()


def set_teacher_class(class_id: UUID, professor_id: UUID, db: Session):
    db.query(Classes).filter(Classes.id == class_id).update(
        {Classes.professor_id: professor_id})
    db.commit()
    return db.query(Classes).filter(Classes.id == class_id).first()


def count_students_class(class_id: UUID, db: Session):
    return db.query(StudentsClasses).filter(StudentsClasses.class_id == class_id).count()
