from pydantic import Field
from typing import Optional
from domain.models.classes import Classes
from domain.schemas.generic import GenericModel
from uuid import UUID
from datetime import date


class StudentData(GenericModel):
    name: str = Field(title="Student's name", Example="Jane Doe")
    birth_date: date | None = Field(
        default=None, title="Student's birth date", Example="1995-12-28")
    cpf: str = Field(title="Student's CPF",
                     regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}', Example="123.456.789-01")
    rg_number: str = Field(
        title="Student's Identity card number", Example="123456-7")
    rg_expediter: str = Field(
        title="Student's Identity card expediter", Example="SEDS")
    rg_uf: str = Field(title="Student's Identity card state",
                       min_length=2, max_length=2, Example="AL")


class Student(StudentData):
    id: UUID = Field(title="Student's ID", Example="UUID")
    course_id: UUID | None = Field(title="Student's course ID", Example="UUID")
    classes: Optional[list] | None = Field(title="List of student's classes")

    class Config:
        orm_mode = True
