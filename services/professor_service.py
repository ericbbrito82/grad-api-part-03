from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.models.professors import Professors

from domain.schemas.professor import ProfessorData

from repositories import professor_repository

from uuid import UUID


def get_professors(db: Session):
    return professor_repository.get_professors(db)


def get_professor(professor_id: UUID, db: Session):
    f = professor_repository.get_professor(professor_id, db)
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Professor with id={professor_id} not found')
    return f


def add_professor(professor_data: ProfessorData, db: Session):
    new_professor = Professors(**professor_data.dict())
    return professor_repository.add_professor(new_professor, db)
