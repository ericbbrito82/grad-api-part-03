from pydantic import Field
from domain.schemas.generic import GenericModel
from uuid import UUID
from datetime import date


class CourseData(GenericModel):
    name: str = Field(title="Course's name", Example="Civil Engineering")
    creation_date: date = Field(title="Creation date", Example="1958-06-22")
    bilding: str = Field(title="Course's bilding's name", Example="CTEC")
    coordinator_id: UUID = Field(
        title="Course's coordinator ID", Example="UUID")


class Course(CourseData):
    id: UUID = Field(title="Course ID", Example="UUID")

    class Config:
        orm_mode = True
