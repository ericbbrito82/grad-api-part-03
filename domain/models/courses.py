from sqlalchemy import Column, ForeignKey, String, Date
from sqlalchemy.dialects.postgresql import UUID
import uuid

from domain.models.generic import GenericBase


class Courses(GenericBase):
    __tablename__ = "courses"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, unique=True)
    creation_date = Column(Date)
    bilding = Column(String)
    coordinator_id = Column(UUID(as_uuid=True), ForeignKey("professors.id"))
