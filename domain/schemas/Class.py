from pydantic import Field
from domain.schemas.generic import GenericModel
from uuid import UUID


class ClassData(GenericModel):
    name: str = Field(title="Name of the class", Example="Calculus 101")
    code: str = Field(title="Code of the class", Example="C101")
    description: str = Field(title="Description of the class",
                             Example="It's just math, but more complicated")
    professor_id: UUID = Field(
        title="ID of the professor who teaches this class", Example="UUID")


class Class(ClassData):
    id: UUID = Field(title="Class ID", Example="UUID")

    class Config:
        orm_mode = True


class ClassRank(GenericModel):
    class_name: str = Field(title="Name of the class", Example="Calculus 101")
    number_of_students: int = Field(
        title="Number of students enrolled in this class", Example="5")

    class Config:
        orm_mode = True
